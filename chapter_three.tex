\chapter{Chapter Three: \keys{alt + x} and Basic Commands}

\begin{figure}[H]
  \caption{xkcd 378 \cite{xkcd-378}}
  \includegraphics[width=1\linewidth]{include/images/xkcd_378}
\end{figure}

\section{Commands vs. Shortcuts}
At this point, you might find yourself wondering what exactly the difference
between commands and shortcuts is, if there even is one, and where to use one
or the other.  The answer these questions respectively is: not really and
use whatever works best for you.

Shortcuts are just keybindings for commands.  They can be rebound to something
else (I will show you how to do this in chapter 7), or unbound entirely;
it makes no difference to Emacs.
Commands are what controls everything behind the scenes.  When you press a
keyboard shortcut, say CTRL + x + s, it is really running a command.
In this case, \textcolor{blue}{save-buffer}.

\section{Common Shortcuts and Their Commands}
Below are some of the common shortcuts, with their assosciated commands.

\begin{table}[H]
\caption{Shortcuts/Commands}
\centering
\begin{tabular}{| r | l |}
  \hline % Top of table border
  \multicolumn{1}{| c |}{\textbf{Shortcut}} &
  \multicolumn{1}{c |}{\textbf{Command}} \\
  \hline % Separates heading from table body
  \keys{\ctrl + x + f}                 & \textcolor{blue}{find-file} \\
  \keys{\ctrl + x + s}                 & \textcolor{blue}{save-buffer} \\
  \keys{\ctrl + x} \keys{s}            & \textcolor{blue}{save-some-buffers} \\
  \keys{\ctrl + shift + $\rightarrow$} & \textcolor{blue}{mark-word} \\
  \keys{alt + w}                       & \textcolor{blue}{copy-region-as-kill} \\
  \keys{\ctrl + w}                     & \textcolor{blue}{kill-region} \\
  \keys{\ctrl + y}                     & \textcolor{blue}{yank} \\
  \keys{\ctrl + x} \keys{u}            & \textcolor{blue}{undo} \\
  \keys{\ctrl + s}                     & \textcolor{blue}{isearch-forward} \\
  \keys{\ctrl + r}                     & \textcolor{blue}{isearch-backward} \\
  \keys{\ctrl + x + c}                 & \textcolor{blue}{kill-emacs} \\
  \keys{\ctrl + x + $\rightarrow$}     & \textcolor{blue}{switch-to-next-buffer} \\
  \keys{\ctrl + x + $\leftarrow$}      & \textcolor{blue}{switch-to-prev-buffer} \\
  \keys{\ctrl + x} \keys{b}            & \textcolor{blue}{switch-to-buffer} \\
  \keys{\ctrl + x} \keys{k}            & \textcolor{blue}{kill-buffer} \\
  \keys{\ctrl + x} \keys{3}            & \textcolor{blue}{split-window-right} \\
  \keys{\ctrl + x} \keys{2}            & \textcolor{blue}{split-window-down} \\
  \keys{\ctrl + x} \keys{1}            & \textcolor{blue}{delete-other-windows} \\
  \keys{\ctrl + x} \keys{0}            & \textcolor{blue}{delete-window} \\
  \keys{\ctrl + x} \keys{o}            & \textcolor{blue}{next-multiframe-window} \\
  \keys{alt + z}                       & \textcolor{blue}{zap-to-char} \\
  \keys{\ctrl + x} \keys{(}            & \textcolor{blue}{start-kbd-macro} \\
  \keys{\ctrl + x} \keys{)}            & \textcolor{blue}{end-kbd-macro} \\
  \hline % End of table border
\end{tabular}
\end{table}

\section{Entering Commands}
To begin entering commands, press \keys{alt + x}.  You should see the prompt
\textcolor{olive}{M-x} in the minibuffer with a blinking cursor next to it.
This means that Emacs is ready to accept a command.  To test this, type the
command \textcolor{blue}{mark-word} and press \keys{enter}.  If you have a
non-empty buffer open, the word to the right of the cursor should be selected
or 'marked'.  You might notice that if you move the cursor after this it
continues highlighting.  To cancel the selection, press \keys{\ctrl + g}.

The example above shows the pattern of how commands work in Emacs:
\begin{list}{\labelitemi}{\leftmargin=1em}
\item 1. Type \keys{alt + x}
\item 2. Type the name of the command and press \keys{enter}
\item 2a. The command asks you for more information if necessary
\item 3. The command runs
\end{list}

To get an idea of just how many commands Emacs has, press \keys{alt + x}
\keys{tab}.  Keep pressing tab until it circles back around to 'a', then type
\keys{\ctrl + g}.  If the number of commands seems intimidating to you, don't
worry, nobody expects you to memorize all (or even most) of them.  In reality,
you will probably only need a few of them day-to-day.  The rest are just nice
to have for when you need them.

\section{Why Emacs Shows M\hyp{}x in the Mini-Buffer}
The question many beginners find themselves asking when learning about
commands is: 'Why does the prompt use M-x instead of alt+x?'.

The reason for this is the same reason that Emacs uses the kill/yank metaphor
instead of the copy/paste metaphor: it's old.  Back when emacs was first
created, it was designed for the space-cadet keyboard.  Rather than the alt
key, the space-cadet keyboard had a Meta key, hence the 'M'.

\begin{figure}[H]
  \caption{The Space-cadet Keyboard \cite{space-cadet}}
  \includegraphics[width=1\linewidth]{include/images/space_cadet}
\end{figure}

\section{Some Useful Commands}

Below are a few basic commands useful for common tasks.

\subsection{Searching}

\subsubsection{The Different Kinds of Search}
\begin{list}{\labelitemi}{\leftmargin=1em}
\item\textbf{Basic Search:}
The basic search functionality in Emacs works the same way it does in other
programs.  You type the string you want to search for, hit enter, and the
program finds the next instance of that string.  The commands to do this in
Emacs are \textcolor{blue}{search-forward} and \textcolor{blue}{search-backward}.
\item\textbf{I-search:}
In addition to the basic search, Emacs provides what is called an i-search, or
iterative search.  The difference between this and a basic search is that an
i-search begins searching as you type, meaning that you often only have to
type part of the word to find it.  The commands for this are
\textcolor{blue}{isearch-forward} and \textcolor{blue}{isearch-backward}.
\item\textbf{Regular Expression Search:}
The final type of search provided by Emacs is the regular expression search.
This type of search does not search for a specific string, but rather for
anything that matches a pattern.  It could for example be used to find any
string made up of four characters and a number.  In order to express this, you
must use a special syntax.  The query for the above example would become
\textcolor{blue}{[a-z]++++[0-9]+}, meaning for characters between a and z
followed by one character between zero and nine.  Regular expressions will be
covered in more detail in chapter 6; for now, all you need to know is that the
commands for a regular expression search are
\textcolor{blue}{search-forward-regexp},
\textcolor{blue}{search-backward-regexp},
\textcolor{blue}{isearch-forward-regexp}, and
\textcolor{blue}{isearch-backward-regexp}.
\end{list}

\subsection{Replacing}

\subsubsection{The Different Kinds of Replace}
\begin{list}{\labelitemi}{\leftmargin=1em}
\item\textbf{Basic Replace:}
The basic replace works similarly to the basic search: you give it the string
you want to replace, press enter, give it the replacement string, press enter,
and it replaces every instance of that string after the cursor.  The command
for this is \textcolor{blue}{replace-string}.
\item\textbf{Query Replace:}
The disadvantage of the basic replace is that it replaces every instance of
the string without asking whether or not you want them replaced.  This is
where the query replace comes in.  It functions similarly to the simple
replace, but gives a prompt before replacing each instance. If you want it to
replace an instance, simply press 'y', otherwise, press 'n'.  The command for
query replace is \textcolor{blue}{query-replace}.
\item\textbf{Regular Expression Replace:}
Regular expression replace functions the same way the regular expression
search does, but rather than searching for the pattern, it replaces the
pattern with the replacement string.  If, for example, we wanted to replace
every string consisting of four alphabetical characters and one number, we
would still use the same pattern (\textcolor{blue}{[a-z]++++[0-9]+}), but
after we hit enter, we would type the replacement string, say 'quux', and hit
enter again.  Emacs would then replace every string consisting of four letters
and a number with the string quux.  The commands for regular expression
replacement are \textcolor{blue}{replace-regexp} and
\textcolor{blue}{query-replace-regexp}.
\end{list}

\subsection{Spell Check}

\subsubsection{Ispell}
\begin{list}{\labelitemi}{\leftmargin=1em}
\item\textbf{Spellcheck Word:}
In order to check the spelling of a single word, you can use the command
\textcolor{blue}{ispell-word}. It will check the spelling of the word to the
left of the cursor, and if it determines it is incorrect, it will give you the
option to replace it.  You may then type \textcolor{blue}{r} to replace the
word, or \textcolor{blue}{space} to skip the word. If the word is correct, it
will instead display \textcolor{olive}{[WORD] is correct} in the mini-buffer.
\item\textbf{Spellcheck Region:}
Spellchecking a region works the same way spellchecking a single word does,
but over a selected area.  To use it, highlight the text you want to check for
errors, enter the command \textcolor{blue}{ispell-region}, and follow enter
either \textcolor{blue}{r} or \textcolor{blue}{space} for each misspelled
word. If the region contains no misspellings, it will display Spell-checking
region using ispell with default dictionary...done
\item\textbf{Spellcheck Buffer:}
To spellcheck an entire buffer, you can use the command
\textcolor{blue}{ispell-buffer}.  Be aware, however, that, depending on the
document, this might contain a large number of false positives.
% ispell-mode
\item\textbf{Ispell Mode:}
Ispell mode works slightly differently from using ispell to check a single
word, region, or buffer.  Rather than giving the option to replace misspelled
words, it will instead display a message in the mini-buffer telling you if the
previously typed word was incorrect.  It will not display anything for
correctly spelled words.  To enable it, enter the command
\textcolor{blue}{ispell-minor-mode}.  To disable, just re-enter the command.
Modes will be explained in more detail in the next chapter.
\end{list}

\subsubsection{Flyspell}
\begin{list}{\labelitemi}{\leftmargin=1em}
\item\textbf{Flyspell Word:}
Flyspell works similarly to the form of spellcheck commonly found in word
processing software.  Rather than giving the option to replace an incorrectly
spelled word, it places a red zig-zag underline beneath the word.  To check a
single word to the left of the cursor, enter the command
\textcolor{blue}{flyspell-word}.  If it is spelled correctly, nothing will
happen.  To get rid of the underline after correcting the word, re-enter the
command.
\item\textbf{Flyspell Region:}
If you want to check the spelling of a region with flyspell, rather than a
single word, select the text that you want to check the spelling of, and enter
the command \textcolor{blue}{flyspell-region}.  When it is done running, it
will display \textcolor{olive}{Spell Checking completed} in the mini-buffer,
and any misspellings in the selected area will have a red underline.
\item\textbf{Flyspell Buffer:}
To check the spelling for all of the text in a buffer, use the command
\textcolor{blue}{flyspell-buffer}.  Like ispell-buffer, this may result in a
large number of false positives.
\item\textbf{Flyspell Mode:}
You can enable on-the-fly checking with the command
\textcolor{blue}{flyspell-mode}.  It will automatically check for and
underline spelling errors as you type.  To disable flyspell mode and clean up
any underlined text, just re-enter the command.
\end{list}

\section{Practice}

\subsection[Search and Destroy]{Search and \sout{Replace} Destroy}
There are 14 John Connors hidden in the file
include/chapter\_three/search\_and\_destroy.txt. It is your job to find and
eliminate them, but be warned, some of them are sneaky and have mixed up the
capitalization of their name.  Use the query
[jJ]+[oO]+[hH]+[nN]+.[cC]+[oO]+[nN]++[oO]+[rR] to find and terminate them.

\subsection{Spelling Counts}
The file include/chapter\_three/speeling\_essay.txt is full of spelling
errors.  Find and correct them using ispell and/or flyspell.

\section{Something Fun}
The butterfly command referenced in the comic at the beginning of the chapter
was actually included in Emacs, as of version
23.1\cite{Emacs-23.1-Release-Notes}.  See below to learn how to use the command.

\subsection{M-x Butterfly}

\begin{list}{\labelitemi}{\leftmargin=1em}
\item Step 1. Type \keys{alt+x}
\item Step 2. Type \textcolor{blue}{butterfly} and press \textcolor{blue}{enter}
\end{list}

\footnotetext{All shortcuts and commands can be found in the Emacs
  manual\cite{Emacs-Manual}}
